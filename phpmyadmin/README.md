# phpmyadmin

This tool can easy to build up phpmyadmin

## Configuration

 1. use nginx-reverse-proxy to redirect request to container, so you need to start reverse proxy
 2. config the enviroment
   - VIRTUAL_HOST for phpmyadmin EX: phpmyadmin.sshellab.com
   - PMA_HOST for mysql host EX: database.sshellab.com
 3. run `docker-compose up -d`

## Reference

 - [Official Document](https://hub.docker.com/r/phpmyadmin/phpmyadmin/)
